Gulp Frontend Development
=========================

## Install
### Requirements Node & Gulp 4
First you need to enter 'rc' directory, then install npm by using command bellow.
```bash
npm install
```


### Tasks
Create Folder 'dist' on root directory.
#### Launch it

Run your project using command bellow from 'src' directory.

```bash
npm run dev
```

#### Build

When you are happy with your changes, run:

```bash
npm run prod
```
